﻿using Microsoft.AspNet.WebHooks;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Bitbucket2Fogbugz
{
    class Program
    {
        static bool IsUnix = (Path.DirectorySeparatorChar == '/');
        static string GitExeName = IsUnix ? "git" : "git.exe";
        static char PathEnvSeparator = IsUnix ? ':' : ';';
        static int Main(string[] args)
        {
            var asm = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(asm.Location);
            Console.WriteLine($@"Bitbucket2Fogbugz Version {fvi.FileVersion}");
            if (args.Length != 2)
            {
                return -1;
            }

            var repoID = args[0];
            var fogbugzURL = args[1];

            var bitBucketDataString = System.Environment
                .GetEnvironmentVariable("BITBUCKET_PAYLOAD", EnvironmentVariableTarget.Process);
            //var bitBucketDataString = File.ReadAllText("example.json");
            //Console.WriteLine("JSon string:");
            //Console.WriteLine(bitBucketDataString);
            JObject o = JObject.Parse(bitBucketDataString);
            // Information about the specific changes
            foreach (var change in o["push"]["changes"])
            {

                // The previous commit
                BitbucketTarget oldTarget = change["old"]["target"].ToObject<BitbucketTarget>();
                // The new commit
                BitbucketTarget newTarget = change["new"]["target"].ToObject<BitbucketTarget>();
                string newBranchName = change["new"]["name"].ToString();

                Console.WriteLine($@"OLD Target message: {oldTarget.Message}");
                Console.WriteLine($@"NEW Target message: {newTarget.Message}");
                Console.WriteLine($@"NEW Branch Name: {newBranchName}");

                var allhashes = GetCommitHashes(oldTarget.Hash, newTarget.Hash);

                for (int i = 1; i < allhashes.Count; i++)
                {
                    var rev = allhashes[i];

                    var caseNumbers = GetCaseNumbers(rev);
                    if (caseNumbers.Count == 0)
                    {
                        continue;
                    }

                    //retrieve filenames
                    var fileNames = ExecuteGit($@"diff-tree --no-commit-id --name-only -r {rev}").Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                    if (fileNames == null || fileNames.Length == 0)
                    {
                        continue;
                    }

                    var client = new WebClient();
                    foreach (var cn in caseNumbers)
                    {
                        foreach (var fn in fileNames)
                        {
                            if (String.IsNullOrEmpty(fn)) continue;
                            Console.WriteLine("sends:");
                            var fogBugzSubmitURL = fogbugzURL + $@"/cvsSubmit.asp?ixBug={cn}&sFile={fn}&sPrev={allhashes[i-1]}&sNew={rev}&ixRepository={repoID}";
                            Console.WriteLine(fogBugzSubmitURL);
                            Console.WriteLine(client.DownloadString(fogBugzSubmitURL));
                        }
                    }
                }
            }
            return 0;
        }

        private static List<String> GetCaseNumbers(String aHash)
        {
            var commitMessage = ExecuteGit($@"cat-file commit {aHash}");
            var regexString = @"(?:Bug[sz]ID\:)(?:(?:\s*)(\d+)(?:\s*)(?:\,*))*";
            var regex = new Regex(regexString, RegexOptions.IgnoreCase);
            var regexmatch = regex.Match(commitMessage);
            var rm = regexmatch.Groups[regexmatch.Groups.Count - 1]; //take the last group
            var caseNumbers = new List<String>();
            foreach (var item in rm.Captures)
            {
                caseNumbers.Add(item.ToString());
            }
            return caseNumbers;
        }

        private static List<String> GetCommitHashes(string oldest, string latest)
        {
            var result = new List<String>();
            var output = ExecuteGit($@"log {oldest}..{latest}");
            var regexString = @"^commit.([0-9a-fA-F]+)";
            var regex = new Regex(regexString, RegexOptions.IgnoreCase | RegexOptions.Multiline);
            var regexmatch = regex.Match(output);
            var rm = regexmatch.Groups[regexmatch.Groups.Count - 1]; //take the last group
            foreach (var item in rm.Captures)
            {
                result.Add(item.ToString());
            }
            return result;
        }

        private static String exeReturnData;

        private static String ExecuteGit(String aParam)
        {
            return ExecuteExe(GitExeName, aParam);
        }

        private static String ExecuteExe(String aExeName, String aParam)
        {
            try
            {
                exeReturnData = String.Empty;
                using (Process p = new Process())
                {
                    // set start info
                    p.StartInfo = new ProcessStartInfo()
                    {
                        RedirectStandardOutput = true,
                        UseShellExecute = false,
                        WorkingDirectory = Directory.GetCurrentDirectory()
                    };
                    // event handlers for output & error
                    p.OutputDataReceived += P_OutputDataReceived;
                    var enviromentPath = String.Empty;
                    if (IsUnix)
                    {
                        enviromentPath = System.Environment
                            .GetEnvironmentVariable("PATH", EnvironmentVariableTarget.Process);
                    }
                    else
                    {
                        enviromentPath = System.Environment
                            .GetEnvironmentVariable("PATH", EnvironmentVariableTarget.Machine);
                    }
                    Console.WriteLine("Path: " + enviromentPath);
                    var paths = enviromentPath.Split(PathEnvSeparator);
                    var exePath = paths.Select(x => Path.Combine(x, aExeName))
                        .Where(x => File.Exists(x))
                        .FirstOrDefault();

                    if (!string.IsNullOrWhiteSpace(exePath))
                    {
                        if (IsUnix)
                        {
                            p.StartInfo.FileName = exePath;
                        }
                        else
                        {
                            p.StartInfo.FileName = exePath.AddDoubleQuotes();
                        }
                        p.StartInfo.Arguments = aParam;
                        // start process                       
                        p.Start();
                        p.BeginOutputReadLine();
                        //wait
                        p.WaitForExit();
                        return exeReturnData;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
            return null;
        }

        private static void P_OutputDataReceived(object sender, DataReceivedEventArgs e) => exeReturnData += e.Data;
    }

    public static class StringExtensions
    {
        public static string AddDoubleQuotes(this string value) => "\"" + value + "\"";
    }
}
